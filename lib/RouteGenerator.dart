import 'package:beatnet/Home.dart';
import 'package:beatnet/Login.dart';
import 'package:beatnet/Register.dart';
import 'package:beatnet/views/Config.dart';
import 'package:beatnet/views/Message.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => Login());
        break;
      case "/login":
        return MaterialPageRoute(builder: (_) => Login());
        break;
      case "/register":
        return MaterialPageRoute(builder: (_) => Register());
        break;
      case "/home":
        return MaterialPageRoute(builder: (_) => Home());
        break;
      case "/config":
        return MaterialPageRoute(builder: (_) => Configuration());
        break;
      case "/messages":
        return MaterialPageRoute(builder: (_) => MessageView(args));
      default:
        _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Tela não encontrada!1"),
        ),
        body: Center(
          child: Text("Tela Não Encontrada"),
        ),
      );
    });
  }
}
