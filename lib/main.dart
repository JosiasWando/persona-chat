import 'package:beatnet/Login.dart';
import 'package:beatnet/RouteGenerator.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "beatnet",
    home: Login(),
    initialRoute: "/",
    onGenerateRoute: RouteGenerator.generateRoute,
    theme: ThemeData(
      primaryColor: Colors.black,
      accentColor: Colors.white,
      scaffoldBackgroundColor: Color(0xffcc0001)
    ),
  ));
}

