import 'package:beatnet/model/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ContactView extends StatefulWidget {
  @override
  _ContactViewState createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {
  String _uuid;
  String _email;

  _userData() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser user = await auth.currentUser();

    _uuid = user.uid;
    _email = user.email;
  }

  Future<List<User>> getUsers() async {
    Firestore db = Firestore.instance;

    QuerySnapshot snapshot = await db.collection("usuarios").getDocuments();

    List<User> users = List();

    for (var item in snapshot.documents) {
      var data = item.data;
      if (data["email"] == _email) continue;

      User user = User();
      user.id = item.documentID;
      user.name = data["name"];
      user.email = data["email"];
      user.imageUrl = data["imageUrl"];

      users.add(user);
    }

    return users;
  }

  @override
  void initState() {
    _userData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<User>>(
      future: getUsers(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(
              child: Column(
                children: <Widget>[
                  Text("Carregando contatos"),
                  CircularProgressIndicator()
                ],
              ),
            );
            break;
          case ConnectionState.done:
            return Container(
              child: ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (_, index) {
                    List<User> users = snapshot.data;
                    User user = users[index];
                    return ListTile(
                      onTap: () {
                        Navigator.pushNamed(context, "/messages",
                            arguments: user);
                      },
                      contentPadding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                      leading: CircleAvatar(
                        backgroundImage: user.imageUrl != null
                            ? NetworkImage(user.imageUrl)
                            : null,
                      ),
                      title: Text(
                        user.name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    );
                  }),
            );
            break;
          case ConnectionState.none:
          case ConnectionState.active:
        }
      },
    );
  }
}
