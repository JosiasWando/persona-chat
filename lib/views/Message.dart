import 'dart:io';

import 'package:beatnet/model/Conversation.dart';
import 'package:beatnet/model/Message.dart';
import 'package:beatnet/model/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MessageView extends StatefulWidget {
  User user;
  MessageView(this.user);

  @override
  _MessageViewState createState() => _MessageViewState();
}

class _MessageViewState extends State<MessageView> {
  bool _uploudIncoming = false;
  TextEditingController _message = TextEditingController();
  String _userId;
  Firestore db = Firestore.instance;

  _userData() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser user = await auth.currentUser();
    _userId = user.uid;

    // Firestore db = Firestore.instance;
    // DocumentSnapshot snapshot = await db.collection("usuarios")
    // .document(_userId)
    // .get();
  }

  _sendMessage() {
    String messageText = _message.text;
    if (messageText.isNotEmpty) {
      Message message = Message();

      message.idUser = _userId;
      message.message = _message.text;
      message.urlImage = "";
      message.type = "text";
      _saveMessage(widget.user.id, _userId, message);
      _saveMessage(_userId, widget.user.id, message);
      _message.text = "";
      _saveConversation(message);
    }
  }

  _saveConversation(Message message) {
    Conversation convS = Conversation.empaty();

    convS.idSender = _userId;
    convS.idRecipient = widget.user.id;
    convS.message = message.message;
    convS.name = widget.user.name;
    convS.photo = widget.user.imageUrl;
    convS.messageType = message.type;
    convS.save();

    Conversation convR = Conversation.empaty();

    convR.idSender = widget.user.id;
    convR.idRecipient = _userId;
    convR.message = message.message;
    convR.name = widget.user.name;
    convR.photo = widget.user.imageUrl;
    convR.messageType = message.type;
    convR.save();
  }

  _saveMessage(String idRecipient, String idSender, Message message) async {
    await db
        .collection("messages")
        .document(idSender)
        .collection(idRecipient)
        .add(message.toMap());
  }

  _sendPhoto() async {
    File selectedImage;
    final picker = ImagePicker();
    var pickerFile;

    pickerFile = await picker.getImage(source: ImageSource.gallery);
    selectedImage = File(pickerFile.path);
    _uploudImage(selectedImage);
  }

  Future _uploudImage(File image) async {
    _uploudIncoming = true;
    if (image != null) {
      String imageName = DateTime.now().millisecondsSinceEpoch.toString();
      FirebaseStorage storage = FirebaseStorage.instance;
      StorageReference root = storage.ref();
      StorageReference file =
          root.child("messages").child("users").child(imageName + ".jpg");

      StorageUploadTask task = file.putFile(image);
      task.events.listen((event) {
        if (event.type == StorageTaskEventType.progress) {
          setState(() {
            _uploudIncoming = true;
          });
        } else if (event.type == StorageTaskEventType.success) {
          setState(() {
            _uploudIncoming = false;
          });
        }

        task.onComplete.then((value) => _getuUrlImage(value));
      });
    }
  }

  _getuUrlImage(StorageTaskSnapshot snapshot) async {
    String url = await snapshot.ref.getDownloadURL();

    Message message = Message();

    message.idUser = _userId;
    message.message = "";
    message.urlImage = url;
    message.type = "image";
    _saveMessage(widget.user.id, _userId, message);
    _saveMessage(_userId, widget.user.id, message);
    _message.text = "";
  }

  @override
  void initState() {
    _userData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var messageBox = Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10),
              child: TextField(
                controller: _message,
                autofocus: true,
                keyboardType: TextInputType.text,
                style: TextStyle(fontSize: 20),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(32, 8, 32, 8),
                    hintText: "Digite sua mensagem",
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50)),
                    prefixIcon: _uploudIncoming
                        ? CircularProgressIndicator()
                        : IconButton(
                            icon: Icon(Icons.photo_camera),
                            onPressed: _sendPhoto,
                          )),
              ),
            ),
          ),
          FloatingActionButton(
            child: Icon(Icons.send),
            mini: true,
            onPressed: _sendMessage,
          )
        ],
      ),
    );

    var stream = StreamBuilder(
        stream: db
            .collection("messages")
            .document(_userId)
            .collection(widget.user.id)
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: Column(
                  children: <Widget>[
                    Text("Carregando contatos"),
                    CircularProgressIndicator()
                  ],
                ),
              );
              break;
            case ConnectionState.active:
              QuerySnapshot query = snapshot.data;
              if (snapshot.hasError) {
                return Expanded(
                  child: Text("Estar com erro"),
                );
              } else {
                return Expanded(
                  child: ListView.builder(
                      itemCount: query.documents.length,
                      itemBuilder: (context, index) {
                        List<DocumentSnapshot> messagesRecived =
                            query.documents.toList();
                        DocumentSnapshot message = messagesRecived[index];

                        double widthContainer =
                            MediaQuery.of(context).size.width * 0.8;

                        Alignment aling = Alignment.centerRight;
                        Color color = Colors.white;
                        Color textColor = Colors.black;

                        if (_userId != message["idUser"]) {
                          color = Colors.black;
                          aling = Alignment.centerLeft;
                          textColor = Colors.white;
                        }

                        return Align(
                            alignment: aling,
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Container(
                                  width: widthContainer,
                                  padding: EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                      color: color,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: message["type"] == "text"
                                      ? Text(
                                          message["message"],
                                          style: TextStyle(
                                              fontSize: 20, color: textColor),
                                        )
                                      : Image.network(message["urlImage"])),
                            ));
                      }),
                );
              }
              break;
            // case ConnectionState.none:
            // case ConnectionState.active:
            default:
              return Container();
          }
        });

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 16),
              child: CircleAvatar(
                backgroundImage: widget.user.imageUrl != null
                    ? NetworkImage(widget.user.imageUrl)
                    : null,
              ),
            ),
            Text(widget.user.name)
          ],
        ),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          child: SafeArea(
              child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[stream, messageBox],
            ),
          ))),
    );
  }
}
