import 'dart:async';

import 'package:beatnet/model/Conversation.dart';
import 'package:beatnet/model/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ConversationView extends StatefulWidget {
  @override
  _ConversationViewState createState() => _ConversationViewState();
}

class _ConversationViewState extends State<ConversationView> {
  StreamController _stream = StreamController<QuerySnapshot>.broadcast();
  Firestore db = Firestore.instance;
  String _uuid;

  getUserId() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser user = await auth.currentUser();

    _uuid = user.uid;

    _addListennerConversation();
  }

  Stream<QuerySnapshot> _addListennerConversation() {
    final stream = db
        .collection("conversations")
        .document(_uuid)
        .collection("lastConversation")
        .snapshots();

    stream.listen((event) {
      _stream.add(event);
    });

    return stream;
  }

  @override
  void initState() {
    getUserId();
    super.initState();
  }

  @override
  void dispose() {
    _stream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _stream.stream,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                  child: Column(
                children: <Widget>[
                  Text("Carregando conversas"),
                  CircularProgressIndicator()
                ],
              ));
              break;
            case ConnectionState.active:
              if (snapshot.hasError) {
                return Text("Erro ao carregar mensagens");
              } else {
                QuerySnapshot query = snapshot.data;

                return ListView.builder(
                    itemCount: query.documents.length,
                    itemBuilder: (context, index) {
                      var conversations = query.documents.toList();
                      DocumentSnapshot conversation = conversations[index];

                      User user = User();
                      user.name = conversation["name"];
                      user.imageUrl = conversation["photo"];
                      user.id = conversation["idRecipient"];

                      return ListTile(
                        onTap: () {
                          Navigator.pushNamed(context, "/messages",
                              arguments: user);
                        },
                        contentPadding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(conversation["photo"]),
                        ),
                        title: Text(
                          conversation["name"],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        subtitle: Text(
                          conversation["message"],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14),
                        ),
                      );
                    });
              }
              break;
            default:
              return Container();
              break;
          }
        });
  }
}
