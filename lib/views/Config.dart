import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import "dart:io";

class Configuration extends StatefulWidget {
  @override
  _ConfigurationState createState() => _ConfigurationState();
}

class _ConfigurationState extends State<Configuration> {
  TextEditingController _name = TextEditingController();
  File _image;
  String _userId;
  bool _uploudIncoming = false;
  String _imageUrl;

  Future _getImage(String oring) async {
    final picker = ImagePicker();
    var pickerFile;

    switch (oring) {
      case "camera":
        pickerFile = await ImagePicker().getImage(source: ImageSource.camera);
        break;
      case "galeria":
        pickerFile = await ImagePicker().getImage(source: ImageSource.gallery);
        break;
    }

    setState(() {
      _image = File(pickerFile.path);
      if (_image != null) {
        _uploudIncoming = true;
        _uploudImage();
      }
    });
  }

  Future _uploudImage() {
    if (_image != null) {
      FirebaseStorage storage = FirebaseStorage.instance;
      StorageReference root = storage.ref();
      StorageReference file = root.child("profile").child(_userId + ".jpg");

      StorageUploadTask task = file.putFile(_image);
      task.events.listen((event) {
        if (event.type == StorageTaskEventType.progress) {
          setState(() {
            _uploudIncoming = true;
          });
        } else if (event.type == StorageTaskEventType.success) {
          setState(() {
            _uploudIncoming = false;
          });
        }

        task.onComplete.then((value) => _getuUrlImage(value));
      });
    }
  }

  Future _getuUrlImage(StorageTaskSnapshot snapshot) async {
    String url = await snapshot.ref.getDownloadURL();
    _updateUrlImageFirestore(url);
    setState(() {
      _imageUrl = url;
    });
  }

  _updateUrlImageFirestore(String url) {
    Firestore db = Firestore.instance;

    db.collection("usuarios").document(_userId).updateData({"imageUrl": url});
  }

  _updateNameFirestore() {
    Firestore db = Firestore.instance;
    String newName = _name.text;
    db.collection("usuarios").document(_userId).updateData({"name": newName});
  }

  _userData() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser user = await auth.currentUser();
    _userId = user.uid;

    Firestore db = Firestore.instance;
    DocumentSnapshot snapshot = await db.collection("usuarios")
    .document(_userId)
    .get();

    var data = snapshot.data;
    _name.text = data["name"];
    if(data["imageUrl"] != null){
      setState(() {
        _imageUrl = data["imageUrl"];
      });
    }
  }

  @override
  void initState() {
    _userData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Config"),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _uploudIncoming ? CircularProgressIndicator() : Container(),
                  CircleAvatar(
                      radius: 100,
                      backgroundColor: Colors.grey,
                      backgroundImage:
                          _imageUrl != null ? NetworkImage(_imageUrl) : null),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          onPressed: () => _getImage("galeria"),
                          child: Text("Galeria")),
                      FlatButton(
                          onPressed: () => _getImage("camera"),
                          child: Text("Câmera")),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 8),
                    child: TextField(
                      controller: _name,
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(32, 16, 32, 16),
                          hintText: "Nome",
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50))),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16, bottom: 10),
                    child: RaisedButton(
                      child: Text(
                        "Salvar",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      color: Colors.black,
                      onPressed: () {
                        _updateNameFirestore();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
