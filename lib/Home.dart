import 'package:beatnet/Login.dart';
import 'package:beatnet/views/Contacts.dart';
import 'package:beatnet/views/Conversations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController _tabs;
  List<String> _popItens = ["Configurações", "Deslogar"];

  Future isLogged() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseUser firebaseLoggedUser = await auth.currentUser();
    if (firebaseLoggedUser == null) {
      Navigator.pushReplacementNamed(context, "/login");
    }
  }

  _LogOff() async {
    FirebaseAuth auth = FirebaseAuth.instance;
    await auth.signOut();
  }

  _popMenu(String choice) async {
    switch (choice) {
      case "Deslogar":
        await _LogOff();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Login()));
        break;
      case "Configurações":
        Navigator.pushNamed(context, "/config");
        break;
    }
  }

  @override
  void initState() {
    _tabs = TabController(length: 2, vsync: this);
    isLogged();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beat Net"),
        bottom: TabBar(
          controller: _tabs,
          tabs: <Widget>[
            Tab(
              text: "Conversas",
            ),
            Tab(
              text: "Contatos",
            )
          ],
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
              onSelected: _popMenu,
              itemBuilder: (context) {
                return _popItens.map((e) {
                  return PopupMenuItem<String>(
                    value: e,
                    child: Text(e),
                  );
                }).toList();
              })
        ],
      ),
      body: TabBarView(
          controller: _tabs,
          children: <Widget>[ConversationView(), ContactView()]),
    );
  }
}
