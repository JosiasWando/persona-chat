class User {
  String id;
  String _name;
  String _email;
  String password;
  String imageUrl;

  User();

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      "name": this.name,
      "email": this.email,
      "imageUrl": this.imageUrl
    };

    return map;
  }
}
