class Message {
  String idUser;
  String message;
  String urlImage;
  String type;

  Message();

  Map<String, dynamic> toMap() {
    var map = {
      "idUser": this.idUser,
      "message": this.message,
      "urlImage": this.urlImage,
      "type": this.type
    };

    return map;
  }
}
