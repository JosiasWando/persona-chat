import 'package:cloud_firestore/cloud_firestore.dart';

class Conversation {
  String id;
  String idSender;
  String idRecipient;
  String messageType;
  String _name;
  String _message;
  String _photo;

  Conversation(this._name, this._message, this._photo);

  Conversation.empaty();

  save() async {
    Firestore db = Firestore.instance;
    await db
        .collection("conversations")
        .document(this.idRecipient)
        .collection("lastConversation")
        .document(this.idSender)
        .setData(this.toMap());
  }

  Map<String, dynamic> toMap() {
    var map = {
      "id": this.id,
      "idSender": this.id,
      "idRecipient": this.idRecipient,
      "messageType": this.messageType,
      "name": this.name,
      "message": this.message,
      "photo": this.photo
    };

    return map;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  String get message => _message;

  String get photo => _photo;

  set photo(String value) {
    _photo = value;
  }

  set message(String value) {
    _message = value;
  }
}
